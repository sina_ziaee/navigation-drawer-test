package com.example.navigatortest;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;

public class AdapterRecycleViewMovie extends RecyclerView.Adapter<AdapterRecycleViewMovie.RecyclerViewHolder> implements View.OnClickListener {

    ArrayList<PrimaryItem> list_movies;
    Context mContext;

    public AdapterRecycleViewMovie(ArrayList<PrimaryItem> list_items){
        this.list_movies = list_items;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        view.setOnClickListener(this);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        PrimaryItem item = list_movies.get(position);

        holder.imageView.setImageResource(item.getImg_res());
        holder.tv_name.setText(item.getName());
        holder.tv_size.setText(item.getRate()+" *");

    }

    @Override
    public int getItemCount() {
        return list_movies.size();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView tv_name;
        TextView tv_size;


        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img_movie);
            tv_name = itemView.findViewById(R.id.tv_name_movie);
            tv_size = itemView.findViewById(R.id.tv_size_movie);
            mContext = itemView.getContext();
        }
    }

    @Override
    public void onClick(View v) {
        TextView tv = v.findViewById(R.id.tv_name_movie);
        Intent intent = new Intent(mContext, ActivityEachItem2.class);
        intent.putExtra("name", tv.getText().toString());
        mContext.startActivity(intent);
    }

}
