package com.example.navigatortest;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterRecycleViewApp extends RecyclerView.Adapter<AdapterRecycleViewApp.RecyclerViewHolder> implements View.OnClickListener {

    ArrayList<PrimaryItem> list_app;
    Context mContext;

    public AdapterRecycleViewApp(ArrayList<PrimaryItem> list_items){
        this.list_app = list_items;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_app, parent, false);
        view.setOnClickListener(this);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        PrimaryItem item = list_app.get(position);

        holder.imageView.setImageResource(item.getImg_res());
        holder.tv_name.setText(item.getName());
        holder.tv_size.setText(item.getRate()+" *");
    }

    @Override
    public int getItemCount() {
        return list_app.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView tv_name;
        TextView tv_size;


        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img_app);
            tv_name = itemView.findViewById(R.id.tv_name_app);
            tv_size = itemView.findViewById(R.id.tv_size_app);
            mContext = itemView.getContext();
        }
    }

    @Override
    public void onClick(View v) {
        TextView tv = v.findViewById(R.id.tv_name_app);
        Intent intent = new Intent(mContext, ActivityEachItem2.class);
        intent.putExtra("name", tv.getText().toString());
        mContext.startActivity(intent);
    }

}
