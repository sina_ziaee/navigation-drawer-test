package com.example.navigatortest;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class Fragment_apps extends Fragment {

    String text;
    int bgColor;

    ArrayList<PrimaryItem> list_items1 = new ArrayList<>();
    ArrayList<PrimaryItem> list_items2 = new ArrayList<>();
    ArrayList<PrimaryItem> list_items3 = new ArrayList<>();
    ArrayList<PrimaryItem> list_items4 = new ArrayList<>();

    ArrayList<String> list_app_name_1;
    ArrayList<String> list_app_name_2;
    ArrayList<String> list_app_name_3;
    ArrayList<String> list_app_name_4;

    ArrayList<Integer> list_app_img_1;
    ArrayList<Integer> list_app_img_2;
    ArrayList<Integer> list_app_img_3;
    ArrayList<Integer> list_app_img_4;

    ArrayList<Integer> list_app_rate_1;
    ArrayList<Integer> list_app_rate_2;
    ArrayList<Integer> list_app_rate_3;
    ArrayList<Integer> list_app_rate_4;

    public static Fragment_home newInstance(String text, int bgColor, ArrayList<String> list_app_name_1,
                                            ArrayList<String> list_app_name_2,ArrayList<String> list_app_name_3,
                                            ArrayList<String> list_app_name_4,ArrayList<Integer> list_app_img_1,
                                            ArrayList<Integer> list_app_img_2,ArrayList<Integer> list_app_img_3,
                                            ArrayList<Integer> list_app_img_4,ArrayList<Integer> list_app_rate_1,
                                            ArrayList<Integer> list_app_rate_2,ArrayList<Integer> list_app_rate_3,
                                            ArrayList<Integer> list_app_rate_4) {
        Bundle args = new Bundle();
        args.putString("text", text);
        args.putInt("bgColor", bgColor);
        args.putStringArrayList("list1_name", list_app_name_1);
        args.putStringArrayList("list2_name", list_app_name_2);
        args.putStringArrayList("list3_name", list_app_name_3);
        args.putStringArrayList("list4_name", list_app_name_4);
        args.putIntegerArrayList("list1_img", list_app_img_1);
        args.putIntegerArrayList("list2_img", list_app_img_2);
        args.putIntegerArrayList("list3_img", list_app_img_3);
        args.putIntegerArrayList("list4_img", list_app_img_4);
        args.putIntegerArrayList("list1_rate", list_app_rate_1);
        args.putIntegerArrayList("list2_rate", list_app_rate_2);
        args.putIntegerArrayList("list3_rate", list_app_rate_3);
        args.putIntegerArrayList("list4_rate", list_app_rate_4);

        Fragment_home fragment = new Fragment_home();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bgColor = ContextCompat.getColor(getContext() , getArguments().getInt("bgColor"));
            text = getArguments().getString("text");
            list_app_name_1 = getArguments().getStringArrayList("list1_name");
            list_app_name_2 = getArguments().getStringArrayList("list2_name");
            list_app_name_3 = getArguments().getStringArrayList("list3_name");
            list_app_name_4 = getArguments().getStringArrayList("list4_name");

            list_app_img_1 = getArguments().getIntegerArrayList("list1_img");
            list_app_img_2 = getArguments().getIntegerArrayList("list2_img");
            list_app_img_3 = getArguments().getIntegerArrayList("list3_img");
            list_app_img_4 = getArguments().getIntegerArrayList("list4_img");

            list_app_rate_1 = getArguments().getIntegerArrayList("list1_rate");
            list_app_rate_2 = getArguments().getIntegerArrayList("list2_rate");
            list_app_rate_3 = getArguments().getIntegerArrayList("list3_rate");
            list_app_rate_4 = getArguments().getIntegerArrayList("list4_rate");

            for (int i=0;i<list_app_name_1.size();i++){
                list_items1.add(new PrimaryItem(list_app_name_1.get(i),list_app_img_1.get(i),list_app_rate_1.get(i)));
            }

            for (int i=0;i<list_app_name_2.size();i++){
                list_items2.add(new PrimaryItem(list_app_name_2.get(i),list_app_img_2.get(i),list_app_rate_2.get(i)));
            }

            for (int i=0;i<list_app_name_3.size();i++){
                list_items3.add(new PrimaryItem(list_app_name_3.get(i),list_app_img_3.get(i),list_app_rate_3.get(i)));
            }

            for (int i=0;i<list_app_name_4.size();i++){
                list_items4.add(new PrimaryItem(list_app_name_4.get(i),list_app_img_4.get(i),list_app_rate_4.get(i)));
            }
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        TextView textView = view.findViewById(R.id.tv_text_movie);
        textView.setText(text);

        fill_recyclers_in_fragment(view);

        view.findViewById(R.id.parent_layout_movie).setBackgroundColor(bgColor);

        return view;
    }

    private void fill_recyclers_in_fragment(View view) {

        RecyclerView recyclerView = view.findViewById(R.id.rec1_in_frag_movie);

        AdapterRecycleViewApp adapter = new AdapterRecycleViewApp(list_items1);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        //****************************************************************************//
        RecyclerView recyclerView2 = view.findViewById(R.id.rec2_in_frag_movie);

        AdapterRecycleViewApp adapter2 = new AdapterRecycleViewApp(list_items2);
        recyclerView2.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        recyclerView2.setLayoutManager(layoutManager2);
        recyclerView2.setAdapter(adapter2);

        //****************************************************************************//
        RecyclerView recyclerView3 = view.findViewById(R.id.rec3_in_frag_movie);

        AdapterRecycleViewApp adapter3 = new AdapterRecycleViewApp(list_items3);
        recyclerView3.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager3 = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        recyclerView3.setLayoutManager(layoutManager3);
        recyclerView3.setAdapter(adapter3);

        //****************************************************************************//
        RecyclerView recyclerView4 = view.findViewById(R.id.rec4_in_frag_movie);

        AdapterRecycleViewApp adapter4 = new AdapterRecycleViewApp(list_items4);
        recyclerView4.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager4 = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        recyclerView4.setLayoutManager(layoutManager4);
        recyclerView4.setAdapter(adapter4);

        //****************************************************************************//

    }

}
