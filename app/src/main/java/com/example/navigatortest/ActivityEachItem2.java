package com.example.navigatortest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

public class ActivityEachItem2 extends AppCompatActivity{

    ImageView imageView;
    TextView tv_name;
    TextView tv_rate;
    TextView tv_description;
    TextView tv_duration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_each_item2);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        imageView = findViewById(R.id.img_search_item);
        tv_name = findViewById(R.id.tv_name_search_item);
        tv_rate = findViewById(R.id.tv_rate_search_item);
        tv_duration = findViewById(R.id.tv_duration_search_item);
        tv_description = findViewById(R.id.tv_description_search_item);

        Intent intent = getIntent();
        String name = intent.getStringExtra("name");

        Log.v("hell", "ok till here");

        Item item = find_each(name);

        tv_name.setText(item.getName());
        tv_rate.setText(item.getRate() + "*");
        tv_duration.setText(item.getSize() + "min");
        tv_description.setText(item.getDescription());
        imageView.setImageResource(item.getImg_res());

    }

    public Item find_each(String content){
        DBOpenHelperItem openHelperItem = MainActivity.mdbOpenHelperItem;
        SQLiteDatabase db = openHelperItem.getReadableDatabase();

        String find_item = "SELECT * FROM " + DatabaseContract.ITEM_TABLE_NAME + " WHERE "
                + DatabaseContract.ITEM_COLUMN_NAME + "='" + content + "'";

        Cursor cursor = db.rawQuery(find_item, null);

        Item item = null;

        if (cursor.moveToFirst()) {

            String name = cursor.getString(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_NAME));
            int res_id = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_IMAGE_RES_ID));
            int size = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_SIZE));
            int show_style = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_STYLE));
            int type = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_APP_OR_MOVIE));
            int rate = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_RATE));
            String description = cursor.getString(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_DESCRIPTION));

            item = new Item(name,size, res_id, show_style, type, description, rate);
            Toast.makeText(this, "Item Found", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Failed to find", Toast.LENGTH_SHORT).show();
        }

        return item;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
