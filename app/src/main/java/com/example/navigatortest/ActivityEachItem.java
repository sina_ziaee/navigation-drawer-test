package com.example.navigatortest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

public class ActivityEachItem extends AppCompatActivity {

    Item item;

    TextView tv_name;
    TextView tv_director;
    TextView tv_rate;
    ImageView imageView;
    LinearLayout linearLayout;

    TextView tv_plot;
    TextView tv_year;
    TextView tv_genre;
    TextView tv_size;
    TextView tv_country;
    TextView tv_actors;

    RequestQueue mQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mQueue = Volley.newRequestQueue(this);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        setContentView(R.layout.activity_each_item);
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String director = intent.getStringExtra("director");
        String year = intent.getStringExtra("year");
        String plot = intent.getStringExtra("plot");
        String rate = intent.getStringExtra("rate");
        String size = intent.getStringExtra("size");
        String genre = intent.getStringExtra("genre");
        String img_res = intent.getStringExtra("img_res");
        String actors = intent.getStringExtra("actors");
        String country = intent.getStringExtra("country");

        Log.v("check" , director);

        tv_name = findViewById(R.id.tv_name_in_item_activity);
        tv_director = findViewById(R.id.tv_director_in_item_activity);
        tv_year = findViewById(R.id.tv_year_in_item_activity);
        tv_genre = findViewById(R.id.tv_genre_in_item_activity);
        tv_size = findViewById(R.id.tv_size_in_item_activity);
        tv_plot = findViewById(R.id.tv_plot_in_item_activity);
        tv_rate = findViewById(R.id.tv_rate_in_item_activity);
        imageView = findViewById(R.id.img_in_item_activity);
        tv_country = findViewById(R.id.tv_country_in_item_activity);
        tv_actors = findViewById(R.id.tv_actors_in_item_activity);

        tv_name.setText(name);
        tv_director.append(director);
        tv_year.append(year);
        tv_genre.setText(genre);
        tv_size.setText(size);
        tv_plot.setText(plot);
        tv_rate.append(rate);
        tv_actors.append(actors);
        tv_country.setText(country);

        ImageRequest request_img = new ImageRequest(img_res, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                imageView.setImageBitmap(response);
            }
        }, 120, 120, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.ARGB_8888,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ActivityEachItem.this, "Error : " + error.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });

        mQueue.add(request_img);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
