package com.example.navigatortest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DBOpenHelperItem extends SQLiteOpenHelper {



    public DBOpenHelperItem(@Nullable Context context, int version) {
        super(context, DatabaseContract.DB_NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table_app = "CREATE TABLE " + DatabaseContract.ITEM_TABLE_NAME + "("
                + DatabaseContract.ITEM_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                + DatabaseContract.ITEM_COLUMN_NAME + " TEXT NOT NULL UNIQUE, "
                + DatabaseContract.ITEM_COLUMN_STYLE + " INTEGER NOT NULL , "
                + DatabaseContract.ITEM_COLUMN_IMAGE_RES_ID + " INTEGER NOT NULL " + " , "
                + DatabaseContract.ITEM_COLUMN_SIZE + " INTEGER NOT NULL " + " , "
                + DatabaseContract.ITEM_COLUMN_APP_OR_MOVIE + " INTEGER NOT NULL , "
                + DatabaseContract.ITEM_COLUMN_DESCRIPTION + " TEXT NOT NULL , "
                + DatabaseContract.ITEM_COLUMN_RATE + " TEXT not NULL "+ " );";

        db.execSQL(create_table_app);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (newVersion > oldVersion){
            dropTable(db);
            onCreate(db);
        }

    }

    private void dropTable(SQLiteDatabase db) {
        String drop_query_app = "DROP TABLE IF EXISTS " + DatabaseContract.ITEM_TABLE_NAME;
        db.execSQL(drop_query_app);
    }

    public void save_in_table_item(Item item){
        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseContract.ITEM_COLUMN_NAME, item.getName());
        contentValues.put(DatabaseContract.ITEM_COLUMN_STYLE, item.show_style);
        contentValues.put(DatabaseContract.ITEM_COLUMN_SIZE, item.getSize());
        contentValues.put(DatabaseContract.ITEM_COLUMN_IMAGE_RES_ID, item.getImg_res());
        contentValues.put(DatabaseContract.ITEM_COLUMN_APP_OR_MOVIE, item.getType());
        contentValues.put(DatabaseContract.ITEM_COLUMN_DESCRIPTION, item.getDescription());
        contentValues.put(DatabaseContract.ITEM_COLUMN_RATE, item.getRate());

        SQLiteDatabase database = getWritableDatabase();
        long id = database.insert(DatabaseContract.ITEM_TABLE_NAME, null, contentValues);
    }

    public Item find_specific_item(String name) {
        String select_item = "SELECT * FROM "
                + DatabaseContract.ITEM_TABLE_NAME + " WHERE " + DatabaseContract.ITEM_COLUMN_NAME
                + "='" + name + "';";

        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(select_item, null);

        if (cursor.moveToFirst()) {
            int res_id = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_IMAGE_RES_ID));
            int size = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_SIZE));
            int show_style = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_STYLE));
            int type = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_APP_OR_MOVIE));
            int rate = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_RATE));
            String description = cursor.getString(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_DESCRIPTION));

            Log.v("check", "success");
            Item item = new Item(name, size,res_id,show_style,type,description,rate);
            return item;
        }
        Log.v("check", "failed");
        return null;
    }

}
