package com.example.navigatortest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.io.Serializable;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener{

    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;

    BottomNavigationView bottom_nav;

//    ArrayList<Item> list_app_1;
//    ArrayList<Item> list_app_2;
//    ArrayList<Item> list_app_3;
//    ArrayList<Item> list_app_4;

    ArrayList<String> list_app_name_1;
    ArrayList<Integer> list_app_img_1;
    ArrayList<Integer> list_app_rate_1;
    ArrayList<String> list_app_name_2;
    ArrayList<Integer> list_app_img_2;
    ArrayList<Integer> list_app_rate_2;
    ArrayList<String> list_app_name_3;
    ArrayList<Integer> list_app_img_3;
    ArrayList<Integer> list_app_rate_3;
    ArrayList<String> list_app_name_4;
    ArrayList<Integer> list_app_img_4;
    ArrayList<Integer> list_app_rate_4;

    ArrayList<String> list_movie_name_1;
    ArrayList<Integer> list_movie_img_1;
    ArrayList<Integer> list_movie_rate_1;
    ArrayList<String> list_movie_name_2;
    ArrayList<Integer> list_movie_img_2;
    ArrayList<Integer> list_movie_rate_2;
    ArrayList<String> list_movie_name_3;
    ArrayList<Integer> list_movie_img_3;
    ArrayList<Integer> list_movie_rate_3;
    ArrayList<String> list_movie_name_4;
    ArrayList<Integer> list_movie_img_4;
    ArrayList<Integer> list_movie_rate_4;

//    ArrayList<Item> list_movie_1;
//    ArrayList<Item> list_movie_2;
//    ArrayList<Item> list_movie_3;
//    ArrayList<Item> list_movie_4;


    static DBOpenHelperItem mdbOpenHelperItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mdbOpenHelperItem = new DBOpenHelperItem(this, 1);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences preferences1 = getSharedPreferences("name",MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("name", "value");

//        fill_table_movie();
//        fill_table_app();

        bottom_nav = findViewById(R.id.navigation_bottom);
        bottom_nav.setOnNavigationItemSelectedListener(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        find_all_from_item_table();

        if(savedInstanceState == null){
            bottom_nav.setSelectedItemId(R.id.nav_bottom_home);
        }

        // drawer and toolbar needs to be synchronised so we pass them
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();// takes care of rotating the hamburger icon //google play doesn't have this :)



    }

    private void fill_table_app() {
        mdbOpenHelperItem.save_in_table_item( new Item("Google chrome", 200, R.raw.app_google_chrome, 1, 1, "description", 4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google drive", 200, R.raw.app_google_drive, 1, 1, "description", 4));
        mdbOpenHelperItem.save_in_table_item( new Item("Dropbox", 200, R.raw.app_google_dropbox, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Gmail", 200, R.raw.app_google_gmail, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google google", 200, R.raw.app_google_google, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Icloud", 200, R.raw.app_google_icloud, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google maps", 200, R.raw.app_google_maps, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google onedrive", 200, R.raw.app_google_onedrive, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google play", 200, R.raw.app_google_play, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google sheets", 200, R.raw.app_google_sheets, 1, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google translate", 200, R.raw.app_google_translate, 1, 1, "description",4));

        mdbOpenHelperItem.save_in_table_item( new Item("Google excel", 200, R.raw.app_microsoft_excel, 2, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Adobe illustrator", 200, R.raw.app_microsoft_illustrator, 2, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Adobe indesign", 200, R.raw.app_microsoft_indesign, 2, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Microsoft onedrive", 200, R.raw.app_microsoft_onedrive, 2, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Adobe Photoshop", 200, R.raw.app_microsoft_photoshop, 2, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("PowerPoint", 200, R.raw.app_microsoft_presentation, 2, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Microsoft Store", 200, R.raw.app_microsoft_store, 2, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Microsoft Word", 200, R.raw.app_microsoft_word, 2, 1, "description",4));

        mdbOpenHelperItem.save_in_table_item( new Item("Android", 200, R.raw.app_popular_android, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Apple", 200, R.raw.app_popular_apple, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("App store", 200, R.raw.app_popular_appstore, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Bing", 200, R.raw.app_popular_bing, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Designer", 200, R.raw.app_popular_designer, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Microsoft Edge", 200, R.raw.app_popular_edge, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Fire Fox", 200, R.raw.app_popular_firefox, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Authenticate", 200, R.raw.app_popular_google_authenticator, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Linux", 200, R.raw.app_popular_linux, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Opera", 200, R.raw.app_popular_opera, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Skrill", 200, R.raw.app_popular_skrill, 3, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("VLC media player", 200, R.raw.app_popular_vlc, 3, 1, "description",4));

        mdbOpenHelperItem.save_in_table_item( new Item("Ask Me", 200, R.raw.app_social_askme, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Facebook", 200, R.raw.app_social_facebook, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Google plus", 200, R.raw.app_social_google_plus, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Instagram", 200, R.raw.app_social_instagram, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Linkedin", 200, R.raw.app_social_linkedin, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Messenger", 200, R.raw.app_social_messenger, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Pinterest", 200, R.raw.app_social_pinterest, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Reddit", 200, R.raw.app_social_reddit, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Skype", 200, R.raw.app_social_skype, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Snapchat", 200, R.raw.app_social_snapchat, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Tumbler", 200, R.raw.app_social_tumblr, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Twitter", 200, R.raw.app_social_twitter, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Vine", 200, R.raw.app_social_vine, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Whats App", 200, R.raw.app_social_whatsapp, 4, 1, "description",4));
        mdbOpenHelperItem.save_in_table_item( new Item("Youtube", 200, R.raw.app_social_youtube, 4, 1, "description",4));

    }

    private void fill_table_movie(){
        mdbOpenHelperItem.save_in_table_item(new Item("Ant man", 200, R.raw.movie_ant_man, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Avengers", 200, R.raw.movie_marvel_avengers, 1,2, "temp description",8));
        mdbOpenHelperItem.save_in_table_item(new Item("Age Of Ultron", 200, R.raw.movie_marvel_avengers_two, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Endgame", 200, R.raw.movie_marvel_avengers_end_game, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Winter Soldier", 200, R.raw.movie_marvel_captain_america_two, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Civil War", 200, R.raw.movie_marvel_captain_america_three, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Captain America", 200, R.raw.movie_marvel_captain_marvel, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Gaurdians of Galaxy", 200, R.raw.movie_marvel_guardians_of_the_galaxy, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Hulk", 200, R.raw.movie_marvel_hulk, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Iron man", 200, R.raw.movie_marvel_iron_man, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Iron man 2", 200, R.raw.movie_marvel_iron_man_two, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Iron man 3", 200, R.raw.movie_marvel_iron_man_three, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Thor", 200, R.raw.movie_marvel_thor, 1,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Thor ragnarok", 200, R.raw.movie_marvel_thor_two, 1,2, "temp description",9));

        mdbOpenHelperItem.save_in_table_item(new Item("Band of brothers", 200, R.raw.movie_tv_band_of_brothers, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Breaking bad", 200, R.raw.movie_tv_breaking_bad, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Fargo", 200, R.raw.movie_tv_fargo, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Friends", 200, R.raw.movie_tv_friends, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Game of thrones", 200, R.raw.movie_tv_game_of_thrones, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("House of cards", 200, R.raw.movie_tv_house_of_cards, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Planet two", 200, R.raw.movie_tv_planet_two, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Rick and morty", 200, R.raw.movie_tv_rick_and_morty, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Sherlok", 200, R.raw.movie_tv_sherlok, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Stranger things", 200, R.raw.movie_tv_stranger_things, 2,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("True detective", 200, R.raw.movie_tv_true_detective, 2,2, "temp description",9));

        mdbOpenHelperItem.save_in_table_item(new Item("Fight club", 200, R.raw.movie_top_fight_club, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Inception", 200, R.raw.movie_top_inception, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Memento", 200, R.raw.movie_top_memento, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Saving private Rayan", 200, R.raw.movie_top_saving_private_rayan, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Star wars", 200, R.raw.movie_top_star_wars, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Dark night rises", 200, R.raw.movie_top_the_dark_knight_rises, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Godfather", 200, R.raw.movie_top_the_godfather, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("The good the bad the ugly", 200, R.raw.movie_top_the_good_the_bad_the_ugly, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Lord of the rings", 200, R.raw.movie_top_the_lork_of_the_rings, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("The matrix", 200, R.raw.movie_top_the_matrix, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("The Shawshank redemption", 200, R.raw.movie_top_the_redemption_of_shawosheng, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("The scilence of the lamps", 200, R.raw.movie_top_the_scilence_of_the_lamps, 3,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Twelve angry man", 200, R.raw.movie_top_twelve_angry_man, 3,2, "temp description",9));

        mdbOpenHelperItem.save_in_table_item(new Item("Avatar", 200, R.raw.movie_selling_avatar, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Black panter", 200, R.raw.movie_selling_black_panter, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Furious 7", 200, R.raw.movie_selling_furious_seven, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Harry Potter", 200, R.raw.movie_selling_harry_potter, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Incredibles 2", 200, R.raw.movie_selling_incredibles_two, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Jurassic world", 200, R.raw.movie_selling_jurassic_world, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Skyfall", 200, R.raw.movie_selling_skyfall, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Star wars last Jedi", 200, R.raw.movie_selling_star_wars, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Titanic", 200, R.raw.movie_selling_titanic, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Toy story", 200, R.raw.movie_selling_toy_story_four, 4,2, "temp description",9));
        mdbOpenHelperItem.save_in_table_item(new Item("Transformers", 200, R.raw.movie_selling_transformers, 4,2, "temp description",9));

    }

    //this is because we don't want to get out of the app while the drawer is open so
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment_home fragment = null;

        switch (item.getItemId()){

            case R.id.nav_message:
                Toast.makeText(this, "Message Selected", Toast.LENGTH_SHORT).show();
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_profile:
                Toast.makeText(this, "Profile Selected", Toast.LENGTH_SHORT).show();
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_chat:
                Toast.makeText(this, "Chat Selected", Toast.LENGTH_SHORT).show();
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.nav_share:
                Toast.makeText(this, "Share Selected", Toast.LENGTH_SHORT).show();
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_send:
                Toast.makeText(this, "Send Selected", Toast.LENGTH_SHORT).show();
                drawer.closeDrawer(GravityCompat.START);
                break;

//********************************* bottom navigation options ************************************//
            case R.id.nav_bottom_home:
                fragment = Fragment_home.newInstance("HOME" , R.color.bgColor1,list_app_name_1,
                        list_app_name_2,list_movie_name_1,list_movie_name_2,list_app_img_1,
                        list_app_img_2,list_movie_img_1,list_movie_img_2,list_app_rate_1,
                        list_app_rate_2,list_movie_rate_1,list_movie_rate_2);

                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                break;
            case R.id.nav_bottom_search:

                break;
            case R.id.nav_bottom_movies:
                fragment = Fragment_movies.newInstance("Movies" , R.color.bgColor3,list_movie_name_1,list_movie_name_2
                ,list_movie_name_3,list_movie_name_4,list_movie_img_1,list_movie_img_2,list_movie_img_3,list_movie_img_4
                ,list_movie_rate_1,list_movie_rate_2,list_movie_rate_3,list_movie_rate_4);

                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                break;

            case R.id.nav_bottom_apps:
                fragment = Fragment_apps.newInstance("Apps" , R.color.bgColor4,list_app_name_1,list_app_name_2,
                        list_app_name_3,list_app_name_4,list_app_img_1,list_app_img_2,list_app_img_3,list_app_img_4,
                        list_app_rate_1,list_app_rate_2,list_app_rate_3,list_app_rate_4);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                break;
            default:
                fragment = null;
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
        }

        return true;
    }

    public void find_all_from_item_table(){

        list_app_name_1 = new ArrayList<>();
        list_app_name_2 = new ArrayList<>();
        list_app_name_3 = new ArrayList<>();
        list_app_name_4 = new ArrayList<>();

        list_movie_name_1 = new ArrayList<>();
        list_movie_name_2 = new ArrayList<>();
        list_movie_name_3 = new ArrayList<>();
        list_movie_name_4 = new ArrayList<>();

        list_app_img_1 = new ArrayList<>();
        list_app_img_2 = new ArrayList<>();
        list_app_img_3 = new ArrayList<>();
        list_app_img_4 = new ArrayList<>();

        list_movie_img_1 = new ArrayList<>();
        list_movie_img_2 = new ArrayList<>();
        list_movie_img_3 = new ArrayList<>();
        list_movie_img_4 = new ArrayList<>();

        list_app_rate_1 = new ArrayList<>();
        list_app_rate_2 = new ArrayList<>();
        list_app_rate_3 = new ArrayList<>();
        list_app_rate_4 = new ArrayList<>();

        list_movie_rate_1 = new ArrayList<>();
        list_movie_rate_2 = new ArrayList<>();
        list_movie_rate_3 = new ArrayList<>();
        list_movie_rate_4 = new ArrayList<>();

        String select_all = "SELECT * FROM " + DatabaseContract.ITEM_TABLE_NAME;
        SQLiteDatabase db = mdbOpenHelperItem.getReadableDatabase();
        Cursor cursor = db.rawQuery(select_all, null);

        if (cursor.moveToFirst()){
            do {

                String name = cursor.getString(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_NAME));
                int res_id = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_IMAGE_RES_ID));
                int size = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_SIZE));
                int show_style = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_STYLE));
                int type = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_APP_OR_MOVIE));
                int rate = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_RATE));
                String description = cursor.getString(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_DESCRIPTION));
                switch (show_style){
                    case 1:
                        if (type == 1){
//                            list_app_1.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_app_name_1.add(name);
                            list_app_img_1.add(res_id);
                            list_app_rate_1.add(rate);
                        }
                        else{
//                            list_movie_1.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_movie_name_1.add(name);
                            list_movie_img_1.add(res_id);
                            list_movie_rate_1.add(rate);
                        }
                        break;
                    case 2:
                        if (type == 1){
//                            list_app_2.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_app_name_2.add(name);
                            list_app_img_2.add(res_id);
                            list_app_rate_2.add(rate);
                        }
                        else{
//                            list_movie_2.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_movie_name_2.add(name);
                            list_movie_img_2.add(res_id);
                            list_movie_rate_2.add(rate);
                        }                        break;
                    case 3:
                        if (type == 1){
//                            list_app_3.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_app_name_3.add(name);
                            list_app_img_3.add(res_id);
                            list_app_rate_3.add(rate);
                        }
                        else{
//                            list_movie_3.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_movie_name_3.add(name);
                            list_movie_img_3.add(res_id);
                            list_movie_rate_3.add(rate);
                        }                        break;
                    case 4:
                        if (type == 1){
//                            list_app_4.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_app_name_4.add(name);
                            list_app_img_4.add(res_id);
                            list_app_rate_4.add(rate);
                        }
                        else{
//                            list_movie_4.add(new Item(name, size, res_id, show_style,type, description,rate));
                            list_movie_name_4.add(name);
                            list_movie_img_4.add(res_id);
                            list_movie_rate_4.add(rate);
                        }                        break;
                }

            }while (cursor.moveToNext());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.search_option){
            Intent intent = new Intent(MainActivity.this, ActivitySearch.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
