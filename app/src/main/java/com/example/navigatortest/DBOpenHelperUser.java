package com.example.navigatortest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBOpenHelperUser extends SQLiteOpenHelper {
    public DBOpenHelperUser(@Nullable Context context, int version) {
        super(context, DatabaseContract.DB_NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table = "CREATE TABLE IF NOT EXISTS " + DatabaseContract.USER_TABLE_NAME + " ( "
                + DatabaseContract.USER_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                + DatabaseContract.USER_COLUMN_NAME + " TEXT NOT NULL UNIQUE , "
                + DatabaseContract.USER_COLUMN_USERNAME + " TEXT  PRIMARY KEY, "
                + DatabaseContract.USER_COLUMN_PASSWORD + " TEXT NOT NULL , "
                + DatabaseContract.USER_COLUMN_EMAIL + " TEXT NOT NULL );";

        db.execSQL(create_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion){
            dropTable(db);
            onCreate(db);
        }
    }

    private void dropTable(SQLiteDatabase db) {
        String drop_table = "DROP TABLE IF EXISTS " + DatabaseContract.USER_TABLE_NAME;
        db.execSQL(drop_table);
    }
}
