package com.example.navigatortest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ActivitySearch extends AppCompatActivity implements Serializable, CompoundButton.OnCheckedChangeListener {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Context mContext;
    RequestQueue mQueue;

    SQLiteDatabase db;
    DBOpenHelperItem mOpenHelper;

    RecyclerView recyclerView;

    Adapter_in_search adapter;

    SwitchCompat switchCompat;
    TextView tv_name;
    TextView tv_director;
    TextView tv_rate;
    ImageView imageView;
    LinearLayout linearLayout;

    TextView tv_plot;
    TextView tv_year;
    TextView tv_genre;
    TextView tv_size;
    TextView tv_img_res_id;
    TextView tv_country;
    TextView tv_actors;

    ArrayList<Item> list = new ArrayList<>();

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        preferences = getPreferences(MODE_PRIVATE);
        editor = preferences.edit();

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mContext = ActivitySearch.this;

        mQueue = Volley.newRequestQueue(mContext);
        linearLayout = findViewById(R.id.item_container);

        switchCompat = findViewById(R.id.switch_imdb);
        switchCompat.setOnCheckedChangeListener(this);
        recyclerView = findViewById(R.id.search_recycler_view);

        mOpenHelper = MainActivity.mdbOpenHelperItem;
        db = mOpenHelper.getReadableDatabase();

        switchCompat.setChecked(false);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(manager);

        adapter = new Adapter_in_search(list);

        recyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu_in_search_activity, menu);

        MenuItem searchItem = menu.findItem(R.id.search_button_in_search_activity);
        searchItem.setEnabled(true);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                tv_name = findViewById(R.id.tv_name_from_web);
                tv_director = findViewById(R.id.tv_director_from_web);
                tv_plot = findViewById(R.id.tv_plot_from_web);
                tv_genre = findViewById(R.id.tv_genre_from_web);
                tv_year = findViewById(R.id.tv_year_from_web);
                tv_size = findViewById(R.id.tv_size_from_web);
                tv_rate = findViewById(R.id.tv_rate_from_web);
                imageView = findViewById(R.id.img_from_web);
                tv_img_res_id = findViewById(R.id.tv_img_res_from_web);
                tv_country = findViewById(R.id.tv_country_from_web);
                tv_actors = findViewById(R.id.tv_actors_from_web);

                sendParamsGet(query);

                return true;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!switchCompat.isChecked()) {
                    list = find_from_inner_database(newText);
                    refreshDisplay();
                    return true;
                }
                else{
                    return false;
                }

            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void refreshDisplay() {
        if (list == null) list = new ArrayList<>();
        adapter = new Adapter_in_search(list);
        recyclerView.setAdapter(adapter);

    }

    private ArrayList<Item> find_from_inner_database(String keyword){
        ArrayList<Item> list_to_show = new ArrayList<>();
        String find = "Select * FROM " + DatabaseContract.ITEM_TABLE_NAME + " WHERE "
                + DatabaseContract.ITEM_COLUMN_NAME + " LIKE "
                + "'%" + keyword + "%';";


        Cursor cursor = db.rawQuery(find, null);

        String name;
        int rate;
        int size;
        int img_res;
        int type;
        int show_type;
        Item item;
        String description;

        if (cursor.moveToFirst()){
            do {
                name = cursor.getString(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_NAME));
                img_res = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_IMAGE_RES_ID));
                size = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_SIZE));
                rate = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_RATE));
                type = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_APP_OR_MOVIE));
                show_type = cursor.getInt(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_STYLE));
                description = cursor.getString(cursor.getColumnIndex(DatabaseContract.ITEM_COLUMN_DESCRIPTION));
                item = new Item(name,size,img_res,show_type,type,description,rate);
                list_to_show.add(item);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return list_to_show;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void sendParamsGet(String name){
        String url = "https://www.omdbapi.com/?apikey=ba3185c&t=" + name;

        dialog = new ProgressDialog(this);
        dialog.setTitle("wait");
        dialog.setMessage("");
        dialog.setCancelable(true);
        dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(JsonObjectRequest.Method.GET, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String name = response.getString("Title");
                    String year = response.getString("Year");
                    String rate = response.getString("imdbRating");
                    String plot = response.getString("Plot");
                    String director = response.getString("Director");
                    String genre = response.getString("Genre");
                    String img_resource_url = response.getString("Poster");
                    String size = response.getString("Runtime");
                    String country = response.getString("Country");
                    String actors = response.getString("Actors");

                    Log.v("check", name + " " + director);
                    tv_name.setText(name);
                    tv_director.setText(director);
                    tv_rate.setText(rate);
                    tv_plot.setText(plot);
                    tv_genre.setText(genre);
                    tv_size.setText(size);
                    tv_year.setText(year);
                    tv_img_res_id.setText(img_resource_url);
                    tv_country.setText(country);
                    tv_actors.setText(actors);

                    ImageRequest request_img = new ImageRequest(img_resource_url, new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            dialog.dismiss();
                            imageView.setImageBitmap(response);
                        }
                    }, 120, 120, ImageView.ScaleType.FIT_CENTER, Bitmap.Config.ARGB_8888,
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    dialog.dismiss();
                                    Toast.makeText(ActivitySearch.this, "Error : " + error.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                                }
                            });

                    mQueue.add(request_img);

                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (NumberFormatException e){
                    Toast.makeText(mContext, "Bad Name", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mQueue.add(request);

    }

    public void onClick(View view) {
        Toast.makeText(mContext, "succeed", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, ActivityEachItem.class);

        intent.putExtra("name", tv_name.getText().toString());
        intent.putExtra("year",tv_year.getText().toString());
        intent.putExtra("genre",tv_genre.getText().toString());
        intent.putExtra("director",tv_director.getText().toString());
        intent.putExtra("rate",tv_rate.getText().toString());
        intent.putExtra("size",tv_size.getText().toString());
        intent.putExtra("plot",tv_plot.getText().toString());
        intent.putExtra("img_res", tv_img_res_id.getText().toString());
        intent.putExtra("country", tv_country.getText().toString());
        intent.putExtra("actors", tv_actors.getText().toString());

        startActivity(intent);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked){
            linearLayout.setVisibility(LinearLayout.VISIBLE);
            recyclerView.setVisibility(RecyclerView.GONE);
        }
        else{
            linearLayout.setVisibility(LinearLayout.GONE);
            recyclerView.setVisibility(RecyclerView.VISIBLE);
        }
    }
}
