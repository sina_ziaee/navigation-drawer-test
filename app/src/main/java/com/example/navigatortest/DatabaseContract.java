package com.example.navigatortest;

public class DatabaseContract {



    public static final String DB_NAME = "Database";

    public static final String ITEM_TABLE_NAME = "items";
    public static final String ITEM_COLUMN_ID = "item_id";
    public static final String ITEM_COLUMN_NAME = "item_name";
    public static final String ITEM_COLUMN_STYLE = "item_style";
    public static final String ITEM_COLUMN_SIZE = "item_size";
    public static final String ITEM_COLUMN_IMAGE_RES_ID = "item_image_id";
    public static final String ITEM_COLUMN_APP_OR_MOVIE = "item_type";
    public static final String ITEM_COLUMN_DESCRIPTION = "item_description";
    public static final String ITEM_COLUMN_RATE = "item_rate";


    public static final String USER_TABLE_NAME = "users";
    public static final String USER_COLUMN_ID = "user_id";
    public static final String USER_COLUMN_NAME = "user_name";
    public static final String USER_COLUMN_EMAIL = "user_email";
    public static final String USER_COLUMN_PASSWORD = "user_password";
    public static final String USER_COLUMN_USERNAME = "user_username";

}
