package com.example.navigatortest;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter_in_search extends RecyclerView.Adapter<Adapter_in_search.RecyclerViewHolder> implements View.OnClickListener {

    ArrayList<Item> list;
    ArrayList<Item> full_list;
    Context mContext;

    public Adapter_in_search(ArrayList<Item> list){
        this.list = list;
        this.full_list = new ArrayList<>(list);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_in_search,
                parent, false);
        view.setOnClickListener(this);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Item item = list.get(position);

        holder.imageView.setImageResource(item.getImg_res());
        holder.tv_name.setText(item.getName());
        holder.tv_size.setText(item.getSize() + " MB");
        holder.tv_rate.setText(item.getRate() + "*");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onClick(View v) {
        TextView tv = v.findViewById(R.id.tv_name_in_search);
        Intent intent = new Intent(mContext, ActivityEachItem2.class);
        intent.putExtra("name", tv.getText().toString());
        mContext.startActivity(intent);
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView tv_name;
        TextView tv_rate;
        TextView tv_size;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_in_search);
            tv_name = itemView.findViewById(R.id.tv_name_in_search);
            tv_rate = itemView.findViewById(R.id.tv_rate_in_search);
            tv_size = itemView.findViewById(R.id.tv_size_in_search);
            mContext = itemView.getContext();

        }

    }

}
