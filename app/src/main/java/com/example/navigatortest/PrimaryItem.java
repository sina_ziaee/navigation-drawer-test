package com.example.navigatortest;

public class PrimaryItem {
    String name;
    int img_res;
    int rate;

    public PrimaryItem(String name, int img_res, int rate) {
        this.name = name;
        this.img_res = img_res;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg_res() {
        return img_res;
    }

    public void setImg_res(int img_res) {
        this.img_res = img_res;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
